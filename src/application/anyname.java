package application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class anyname extends Application{

	
	@Override
	public void start(Stage stage) throws Exception {
		// TODO Auto-generated method stub
		TextField nametf = new TextField();
		DatePicker dob = new DatePicker();
		RadioButton m = new RadioButton("Male");
		RadioButton f = new RadioButton("Female");
		final ToggleGroup tgGroupGender = new ToggleGroup();
		m.setToggleGroup(tgGroupGender);
		f.setToggleGroup(tgGroupGender);
		
		
		CheckBox j = new CheckBox("Java");
		CheckBox dn = new CheckBox("DotNet");
		final ToggleGroup tgGroup = new ToggleGroup();
		
		ToggleButton tb1 = new ToggleButton("Yes");
		tb1.setToggleGroup(tgGroup);

		ToggleButton tb2 = new ToggleButton("No");
		tb2.setToggleGroup(tgGroup);

		
		Button y = new Button("Yes");
		Button n = new Button("No");
//		ChoiceBox cb = new ChoiceBox();
//		cb.getItems().add("Engeneering");
//		cb.getItems().add("MBA");
//		cb.getItems().add("MCA");
//		cb.getItems().add("Graduation");
//		cb.getItems().add("Mitech");
//		cb.getItems().add("Mphil");
//		cb.getItems().add("Plut");
		ObservableList<String> names = FXCollections.observableArrayList("Engineering","MCA","MBA","Graduation","Mitech","Mphil","Plut");
		ListView<String> educationListView = new ListView<String>(names);
 		ComboBox<String> cbo = new ComboBox<>();
		cbo.getItems().addAll("Selangor", "Pahang","Wilayah Persekutuan");
		Button submit = new Button("Submit");
		
		 //Creating a scene object 
			Label lblName = new Label("Name");
			Label lblDOB = new Label("Date of birth");
			Label lblGender = new Label("Gender");
			Label lblReservation = new Label("Reservation");
			Label lblTech = new Label("Technologies Known");
			Label lblEdu = new Label("Educational qualification");
			Label lblLoc = new Label("Location");
			
			GridPane gp  = new GridPane();
			gp.setPadding(new Insets(20, 20, 20, 20)); 
			gp.setVgap(5); 
			gp.setHgap(5);
	    gp.add(lblName, 0, 0);
	    gp.add(nametf, 1, 0);
	    gp.add(lblDOB, 0, 1);
	    gp.add(dob, 1, 1);
	    gp.add(lblGender, 0, 2);
	    gp.add(m, 1, 2);
	    gp.add(f, 2, 2);
	    gp.add(lblReservation, 0, 3);
	    gp.add(tb1,1,3);
	    gp.add(tb2, 2, 3);
	    gp.add(lblTech, 0, 4);
	    gp.add(j, 1, 4);
	    gp.add(dn, 2, 4);
	    gp.add(lblEdu, 0, 5);
	    gp.add(educationListView, 1, 5);
	    gp.add(lblLoc, 0, 6);
	    gp.add(cbo, 1, 6);
	    gp.add(submit, 2, 7);
	    
	    submit.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					// TODO Auto-generated method stub
					ArrayList <String> techno = new ArrayList <String>();
					System.out.println("Name: " +  nametf.getText());
					
					LocalDate date = dob.getValue();
					System.out.println(date.toString());
					String gender="";
					if (m.isSelected()) {
						System.out.println("Gender: Male" );
						gender = "male";
					}
					if (f.isSelected()) {
						System.out.println("Gender: Female" );
						gender = "female";
					}
					
					if (j.isSelected()) {
						System.out.println("Java" );
						techno.add(j.getText());
					}
					if (dn.isSelected()){
						System.out.println("DotNet" );
						techno.add(dn.getText());
					}
					
					Student std1 = new Student(date.toString(),nametf.getText(),gender, techno);
					//our version
//					try {
//						File file = new File("person.txt");
//						PrintWriter pw = new PrintWriter(new FileOutputStream(file,true/* APPEND MODE */ ));
//						pw.println(std1.name);
//						pw.close();
//					}catch(Exception ex) {
//						System.out.println("Something Happened");
//						ex.printStackTrace();
//					}
					
					
					//miss version
					try(PrintWriter output = new PrintWriter(new FileOutputStream(new File("persons.txt"),true)))
					{
						System.out.println(std1.name);
				
						output.print(std1.name+"\n");
						
					}
					catch(Exception e){
						System.out.println("Error!");
					}
					
					
					
				}
	    	
	    });
	    
	      Scene scene = new Scene(gp);  
	      scene.getStylesheets().add("/application/application.css");
	      //Setting title to the Stage 
	      stage.setTitle("Any name"); 
	         
	      //Adding scene to the stage 
	      stage.setScene(scene); 
	         
	      //Displaying the contents of the stage 
	      stage.show(); 
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	

}
